#region - Auf Access Datenbank zugreifen (oledb Datei) 


	//////////////////// Namespace: System.Data.OleDb ////////////////////////////////
	
	// Visual-Studio -> Tools -> Mit Server verbinden -> Access -> Connection-String aus den Eigenschaften kopieren
	
	OleDbConnection con = new OleDbConnection(
	"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=\"Z:\\Daten\\Limbach Datenbanken\\Limbach Gebäudedienste 2014 Tabellen.accdb");

                            OleDbCommand cmd = con.CreateCommand();    
                            con.Open();    
                            cmd.CommandText = "Querry Text im SQL Format";    
                            cmd.Connection = con;    
                            cmd.ExecuteNonQuery();                           
                            con.Close();   

#endregion	
 
#region - SolidColorBrush aus RGB Farbe erstellen

SolidColorBrush yellowBrush = new SolidColorBrush(System.Windows.Media.Color.FromRgb(255, 255, 0));

#endregion

#region - Inhalte (Strings/Charakter) einer Datei mit einem Streamreader suchen und ersetzen
  
	///////////////////////////// Namespace: System.IO //////////////////////////////////////
	
	var lines = File.ReadAllLines(Dateiname und Pfad);
	
					//Einzelne Strings ersetzen
	
				for (int i = 0; i < lines.Count(); ++i) {
				lines[i] = lines[i].Replace("alte Zeichenfolge", "neue Zeichenfolge");
				lines[i] = lines[i].Replace("alte Zeichenfolge2", "neue Zeichenfolge2");
				lines[i] = lines[i].Replace("alte Zeichenfolge3", "neue Zeichenfolge3");
	
				//Gesamte Zeile beim Vorkommen einer bestimmten Zeichenfolge löschen
				
				if (lines[i].Contains("Zeichenfolge"))
				{
					lines[i] = lines[i].Replace(lines[i], "");
					lines[i] = lines[i].Trim();                                
				}
	
					File.WriteAllLines(Dateiname und Pfad, lines);
					
#endregion
#endregion
				 
#region - Dateinamen als String speichern (Auswahl der Datei mittels openFileDialog)

	 using (OpenFileDialog openFileDialog = new OpenFileDialog()) {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "metadata files (*.metadata)|*.metadata";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK) {
                    //Get the path of specified file
                    filePath = openFileDialog.FileName.Substring(openFileDialog.FileName.LastIndexOf("\\")+2);
				}				 
	 }
				 
#endregion

#region - Regex Beispiel

Regex rx = new Regex(@"\d+[.]\d+");

Match match = rx.Match(lines[i], 0);
if (match.Success && lines[i].Contains("QUANTITY"))
{
    string matchtext = match.Value.ToString();
    matchtext = matchtext.Replace(".", "");
    lines[i] = lines[i].Replace(match.Value, matchtext);
}

#endregion

#region - XML Attribute als Liste erstellen und auslesen mit Hilfe von Lambda Zuweisungen und einem XML Deserializer

public static ImportConfigurationList Load(string FileName = "")

{
    string sFilePath = AppDomain.CurrentDomain.BaseDirectory;
    if (!String.IsNullOrEmpty(FileName))
        sFilePath = FileName;

    sFilePath = String.Format("{0}{1}", sFilePath, "ImportConfiguration");

    try
    {
        using (var stream = System.IO.File.OpenRead(sFilePath))
        {
            var serializer = new XmlSerializer(typeof(ImportConfigurationList));
            return serializer.Deserialize(stream) as ImportConfigurationList;
        }
    }
    catch (Exception ex)
    {
        return null;
    }
}

ImportConfigurationList.Load().ImportConfiguration.Where(x => x.ReyherBelegImport == Kreditor && x.Active == true).FirstOrDefault();

#endregion

#region - Error Log Klasse (Ereignisprotokoll)

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DWNReyherImportSage
{
    public static class ErrorLogHelper
    {
        private static System.Diagnostics.EventLog _EventLog;

        public static System.Diagnostics.EventLog EventLog
        {
            get { return _EventLog; }
            set { _EventLog = value; }
        }

        static ErrorLogHelper()
        {
            _EventLog = new System.Diagnostics.EventLog();
            if (!System.Diagnostics.EventLog.SourceExists("DWNSageBelegImport"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "DWNSageBelegImport", "DWNSageBelegImportLog");
            }
            _EventLog.Source = "DWNSageBelegImport";
            _EventLog.Log = "DWNSageBelegImportLog";
        }
    }
}

ErrorLogHelper.EventLog.WriteEntry(String.Format("Dokument mit der Importnummer: {0}-{1} konnte nicht gespeichert werden!",
oSageBeleg.OrderResponse?.ORDERRESPONSE_HEADER[0]?.ORDER_ID, EventLogEntryType.Error));

#endregion