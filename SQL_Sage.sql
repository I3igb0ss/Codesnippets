--Neue Belegart erstellen

begin

//Auftragsbestätigungsklon


INSERT INTO [WJS].[dbo].[KHKVKBelegarten](
   Kennzeichen,Erfassungsart,Bezeichnung,BezeichnungReport,Nummernkreis,NummernkreisArt,
   Gleichgewichtsstatistik,IstInitialbeleg,IstDialoganlage,IstSammelbeleg,RoherloesModus,
   StatistikWirkungUmsatz,StatistikWirkungMenge,MitFibuUebergabe,FibuUebergabeMode,Disposition,
   DispositionWirkung,Lagerbuchung,LagerbuchungWirkung,GGBestellt,GGBestelltWirkung,GGGeliefert,
   GGGeliefertWirkung,GGRetour,GGRetourWirkung,GGBerechnet,GGBerechnetWirkung,
   MEKBerechnung,MitNachweisPflicht,MitGueltigkeit,Archiv,Projekt,Editierbar,[Option],OptionXRM,AnzeigeIndex)  
  
  VALUES (
   'VVW','4000','Werkstattauftrag','Werkstattauftrag','VVA','2','1','-1','-1','0',
   '1','0','0','0','0','-1','1','0','0','-1','1','0','0','0','0','0',
   '0','0','0','0','0','0','-1','0','0','11')
   
   
   //Lieferscheinklon
   
   INSERT INTO [WJS].[dbo].[KHKVKBelegarten](
   Kennzeichen,Erfassungsart,Bezeichnung,BezeichnungReport,Nummernkreis,NummernkreisArt,
   Gleichgewichtsstatistik,IstInitialbeleg,IstDialoganlage,IstSammelbeleg,RoherloesModus,
   StatistikWirkungUmsatz,StatistikWirkungMenge,MitFibuUebergabe,FibuUebergabeMode,Disposition,
   DispositionWirkung,Lagerbuchung,LagerbuchungWirkung,GGBestellt,GGBestelltWirkung,GGGeliefert,
   GGGeliefertWirkung,GGRetour,GGRetourWirkung,GGBerechnet,GGBerechnetWirkung,LBWPositiv,LBWNegativ
   MEKBerechnung,MitNachweisPflicht,MitGueltigkeit,Archiv,Projekt,Editierbar,[Option],OptionXRM,AnzeigeIndex)  
  
  VALUES ('WSA','4000','Werkstattauftrag','Werkstattauftrag','VLL','1','2','-1','-1','0','1','0','1','0','0','-1','-1','-1','-1','0','0','-1','1','0','0','0','0','EA','ZR','-1','-1','0','0','0','-1','0','0','20')

end  
  
--SQL Abfrage C#

begin

using (var reader = Mandant.MainDevice.GenericConnection.ExecuteReader(
$"SELECT Feld1, Feld2 " +
$"FROM Tabelle " ))
{
	while (reader.Read())
	{
		Variable1 = reader.GetString("Feld1");
		Variable2 = reader.GetString("Feld2");									
	}
}

end

--SQL Command C#

begin

Sagede.OfficeLine.Data.IGenericCommand sagecommand1 = 
Mandant.MainDevice.GenericConnection.CreateCommand(
Sagede.OfficeLine.Data.GenericCommandType.SqlString,                                                   				
$"DELETE FROM Tabelle "
, true);

sagecommand1.ExecuteNonQuery();

end

--Zusätzliche Tabellen für Userfelder

begin

INSERT INTO USysSetup(Tree, Token, Property, [Value]) 
VALUES ('UserFields', 'VKBelegarten', 'KHKVKBelegarten', 'Belegarten Verkauf')

end
