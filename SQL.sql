

begin --Alle Columns, die keine Nullwerte zulassen anzeigen ✅

	SELECT 
	COL.COLUMN_NAME, 
	COL.DATA_TYPE, 
	COL.CHARACTER_MAXIMUM_LENGTH, 
	COL.IS_NULLABLE 
	FROM INFORMATION_SCHEMA.COLUMNS COL 
	WHERE COL.TABLE_NAME = 'Tabellenname'
	ORDER BY IS_NULLABLE

end

begin --In der Datenbank nach einer Tabelle mit der Bezeichnung XYZ suchen ✅

	SELECT TABLE_SCHEMA, TABLE_NAME
	FROM INFORMATION_SCHEMA.TABLES
	WHERE TABLE_NAME LIKE '%%'

end

begin --In allen Tabellen nach einer Spalte mit dem Namen *Beispielspalte* suchen (Systemtabellen ausschließen) ✅


	--Wichtig! Auf Master-Ebene ausführen!
	
	DECLARE @ColumnNamePart NVARCHAR(128)
	SET @ColumnNamePart = '%Suchbegriff%' -- Ersetze dies durch den Teil des Spaltennamens, nach dem du suchen möchtest
	
	-- Temp-Tabelle zur Speicherung der Ergebnisse
	IF OBJECT_ID('tempdb..#Results') IS NOT NULL
		DROP TABLE #Results
	
	CREATE TABLE #Results (
		DatabaseName NVARCHAR(128),
		SchemaName NVARCHAR(128),
		TableName NVARCHAR(128),
		ColumnName NVARCHAR(128)
	)
	
	-- Cursor zur Durchlauf aller Datenbanken
	DECLARE db_cursor CURSOR FOR
	SELECT name
	FROM sys.databases
	WHERE state_desc = 'ONLINE'
		AND database_id > 4 -- Systemdatenbanken ausschließen
	
	OPEN db_cursor
	
	DECLARE @DBName NVARCHAR(128)
	DECLARE @SQL NVARCHAR(MAX)
	
	FETCH NEXT FROM db_cursor INTO @DBName
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @SQL = '
		INSERT INTO #Results (DatabaseName, SchemaName, TableName, ColumnName)
		SELECT 
			''' + @DBName + ''' AS DatabaseName,
			SCHEMA_NAME(t.schema_id) AS SchemaName,
			t.name AS TableName,
			c.name AS ColumnName
		FROM ' + QUOTENAME(@DBName) + '.sys.columns c
		INNER JOIN ' + QUOTENAME(@DBName) + '.sys.tables t ON c.object_id = t.object_id
		WHERE c.name LIKE @ColumnNamePart'
		
		EXEC sp_executesql @SQL, N'@ColumnNamePart NVARCHAR(128)', @ColumnNamePart
		
		FETCH NEXT FROM db_cursor INTO @DBName
	END
	
	CLOSE db_cursor
	DEALLOCATE db_cursor
	
	-- Ergebnisse anzeigen
	SELECT * FROM #Results
	
	-- Temp-Tabelle löschen
	DROP TABLE #Results

	end
	
	begin --Deadlocks ermitteln und Transaktionen löschen
	
	exec sp_who2
	
	--dann nach Status blocked by suchen 
	
	Kill 'SPID'

end

BEGIN --Nach Stored Procedure suchen und Datenbankname + Tabellenname(falls vorhanden) ausgeben ✅

	DECLARE @SearchTitle NVARCHAR(100) = 'tblMessagesLaden';
	DECLARE @SQL NVARCHAR(MAX) = '';
	
	SELECT @SQL = @SQL +
	'
	USE [' + name + '];
	IF EXISTS (SELECT * FROM sys.objects WHERE type = ''P'' AND name = ''' + @SearchTitle + ''')
	BEGIN
		PRINT ''Stored procedure found in database: ' + name + ''';
	
		SELECT ''' + name + ''' AS DatabaseName, OBJECT_NAME(object_id) AS ProcedureName, OBJECT_NAME(parent_object_id) AS TableName
		FROM sys.objects
		WHERE type = ''P'' AND name = ''' + @SearchTitle + ''';
		
		EXEC sp_helptext ''' + @SearchTitle + ''';
	END
	'
	FROM sys.databases
	WHERE state_desc = 'ONLINE'; -- Nur Datenbanken im Zustand "ONLINE" berücksichtigen
	
	EXEC sp_executesql @SQL;

END

BEGIN --Nach Begriff in allen Spalten der Tabellen einer Datenbank suchen und den Spalten- und Tabellennamen zurück geben ✅

	DECLARE @SearchTerm NVARCHAR(100) = 'IhrSuchbegriff';

	DECLARE @TableName NVARCHAR(128), @ColumnName NVARCHAR(128), @SQL NVARCHAR(MAX);

	DECLARE TableCursor CURSOR FOR
	SELECT 
		t.name AS TableName, 
		c.name AS ColumnName
	FROM 
		sys.tables t
	JOIN 
		sys.columns c ON t.object_id = c.object_id
	WHERE 
		c.system_type_id IN (SELECT system_type_id FROM sys.types WHERE is_user_defined = 0) -- Filtert Benutzerdefinierte Typen aus
		AND c.collation_name IS NOT NULL -- Filtert unveränderbare Spalten aus (bspw. calculated columns)
	ORDER BY 
		t.name, 
		c.name;

	CREATE TABLE #Results (TableName NVARCHAR(128), ColumnName NVARCHAR(128));

	OPEN TableCursor;

	FETCH NEXT FROM TableCursor INTO @TableName, @ColumnName;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @SQL = N'IF EXISTS (SELECT 1 FROM ' + QUOTENAME(@TableName) + N' WHERE ' + QUOTENAME(@ColumnName) + N' LIKE ''%' + @SearchTerm + '%'') ' +
				   N'INSERT INTO #Results (TableName, ColumnName) VALUES (@TableName, @ColumnName);';

		EXEC sp_executesql @SQL, N'@TableName NVARCHAR(128), @ColumnName NVARCHAR(128)', @TableName, @ColumnName;

		FETCH NEXT FROM TableCursor INTO @TableName, @ColumnName;
	END;

	CLOSE TableCursor;
	DEALLOCATE TableCursor;

	SELECT * FROM #Results;

	DROP TABLE #Results;	

END

BEGIN -- Nach Begriff in allen Spalten der Tabellen einer Datenbank suchen und den Spalten- und Tabellennamen zurück geben (Alternativmethode) ✅

	USE PGIS_BauUndKunst --Datenbankname
	DECLARE @SearchStr nvarchar(100) = 'Voruntersuchung' --Suchbegriff
	DECLARE @Results TABLE (ColumnName nvarchar(370), ColumnValue nvarchar(3630))
	SET NOCOUNT ON
	DECLARE @TableName nvarchar(256), @ColumnName nvarchar(128), @SearchStr2 nvarchar(110)
	SET  @TableName = ''
	SET @SearchStr2 = QUOTENAME('%' + @SearchStr + '%','''')
	WHILE @TableName IS NOT NULL
	BEGIN
    SET @ColumnName = ''
    SET @TableName = 
    (
        SELECT MIN(QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME))
        FROM     INFORMATION_SCHEMA.TABLES
        WHERE         TABLE_TYPE = 'BASE TABLE'
            AND    QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME) > @TableName
            AND    OBJECTPROPERTY(
                    OBJECT_ID(
                        QUOTENAME(TABLE_SCHEMA) + '.' + QUOTENAME(TABLE_NAME)
                         ), 'IsMSShipped'
                           ) = 0
    )
    WHILE (@TableName IS NOT NULL) AND (@ColumnName IS NOT NULL)
    BEGIN
        SET @ColumnName =
        (
            SELECT MIN(QUOTENAME(COLUMN_NAME))
            FROM     INFORMATION_SCHEMA.COLUMNS
            WHERE         TABLE_SCHEMA    = PARSENAME(@TableName, 2)
                AND    TABLE_NAME    = PARSENAME(@TableName, 1)
                AND    DATA_TYPE IN ('char', 'varchar', 'nchar', 'nvarchar', 'int', 'decimal')
                AND    QUOTENAME(COLUMN_NAME) > @ColumnName
        )
        IF @ColumnName IS NOT NULL
        BEGIN
            INSERT INTO @Results
            EXEC
            (
                'SELECT ''' + @TableName + '.' + @ColumnName + ''', LEFT(' + @ColumnName + ', 3630) 
                FROM ' + @TableName + ' (NOLOCK) ' +
                ' WHERE ' + @ColumnName + ' LIKE ' + @SearchStr2
            )
        END
    END    
	END
	SELECT ColumnName, ColumnValue FROM @Results

END

BEGIN --Tabellen in der richtigen Reihenfolge löschen um Verletzungen von Foreign Key Constraints zu vermeiden. (chronologische Auflistung der Löschreihenfolge) ✅

	-- Temporäre Tabelle zur Speicherung der Abhängigkeiten
	CREATE TABLE #Dependencies (
		FK_Table NVARCHAR(128),
		PK_Table NVARCHAR(128)
	);
	
	-- Füllen der temporären Tabelle mit Abhängigkeiten
	INSERT INTO #Dependencies (FK_Table, PK_Table)
	SELECT
		FK.TABLE_NAME,
		PK.TABLE_NAME
	FROM
		INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS AS RC
		INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS FK
			ON RC.CONSTRAINT_NAME = FK.CONSTRAINT_NAME
		INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS PK
			ON RC.UNIQUE_CONSTRAINT_NAME = PK.CONSTRAINT_NAME;
	
	-- Temporäre Tabelle zur Speicherung der Löschreihenfolge
	CREATE TABLE #DeleteOrder (
		TableName NVARCHAR(128),
		OrderLevel INT
	);
	
	DECLARE @CurrentLevel INT = 0;
	DECLARE @RowsInserted INT;
	
	-- Starttabelle festlegen (bitte den Tabellennamen hier anpassen)
	DECLARE @StartTable NVARCHAR(128) = 'IhreStartTabelle';
	
	-- Fügen Sie die Starttabelle hinzu
	INSERT INTO #DeleteOrder (TableName, OrderLevel)
	VALUES (@StartTable, @CurrentLevel);
	
	SET @RowsInserted = @@ROWCOUNT;
	
	WHILE @RowsInserted > 0
	BEGIN
		SET @CurrentLevel = @CurrentLevel + 1;
	
		INSERT INTO #DeleteOrder (TableName, OrderLevel)
		SELECT DISTINCT D.FK_Table, @CurrentLevel
		FROM #Dependencies D
		INNER JOIN #DeleteOrder O ON D.PK_Table = O.TableName
		WHERE D.FK_Table NOT IN (SELECT TableName FROM #DeleteOrder);
	
		SET @RowsInserted = @@ROWCOUNT;
	END
	
	-- Tabellen mit Abhängigkeiten in der richtigen Reihenfolge ausgeben
	SELECT TableName
	FROM #DeleteOrder
	ORDER BY OrderLevel DESC;
	
	-- Bereinigungen
	DROP TABLE #Dependencies;
	DROP TABLE #DeleteOrder;

End

BEGIN --Automatische Trennung vom Server anpassen/deaktivieren MSSQL (z.B. lokale Datenbank))

	EXEC sp_configure 'remote query timeout', 0; --Zeit: z.b. 600 = 10 Minuten
	RECONFIGURE;

END

BEGIN --alle Datenbanken inkl. Tabellen (außer System-Datenbanken bzw. Tabellen) zu löschen - inkl. Server-/Instanzcheck aus Sicherheitsgründen

	DECLARE @ExpectedInstanceName NVARCHAR(255) = 'DEINE_INSTANZNAME'
	DECLARE @ActualInstanceName NVARCHAR(255)
	-- Holen des tatsächlichen Instanznamens
	SET @ActualInstanceName = @@SERVERNAME
	-- Prüfen, ob die aktuelle Instanz mit der erwarteten übereinstimmt
	IF @ActualInstanceName <> @ExpectedInstanceName
	BEGIN
		PRINT 'FEHLER: Dieses Skript wird auf einer falschen Instanz ausgeführt!'
		PRINT 'Erwartete Instanz: ' + @ExpectedInstanceName
		PRINT 'Aktuelle Instanz: ' + @ActualInstanceName
		RETURN
	END
	-- Wenn die Instanzen übereinstimmen, wird das Skript fortgesetzt
	DECLARE @DBName NVARCHAR(255)
	DECLARE @SQL NVARCHAR(255)
	DECLARE db_cursor CURSOR FOR
	SELECT name 
	FROM sys.databases
	WHERE name NOT IN ('master', 'model', 'msdb', 'tempdb')  -- Systemdatenbanken ausschließen
	OPEN db_cursor   
	FETCH NEXT FROM db_cursor INTO @DBName   
	WHILE @@FETCH_STATUS = 0   
	BEGIN   
		SET @SQL = 'ALTER DATABASE [' + @DBName + '] SET SINGLE_USER WITH ROLLBACK IMMEDIATE;'
		EXEC sp_executesql @SQL  
		SET @SQL = 'DROP DATABASE [' + @DBName + '];'
		EXEC sp_executesql @SQL  
		FETCH NEXT FROM db_cursor INTO @DBName   
	END   
	CLOSE db_cursor   
	DEALLOCATE db_cursor
	PRINT 'Alle Nicht-Systemdatenbanken wurden erfolgreich gelöscht.'
	
END

BEGIN --alle Datenbanken eines Servers in einen definierten Pfad sichern

	-- Führe BACKUP und CHECKPOINT ohne Transaktionen aus
	DECLARE @BackupPath NVARCHAR(255) = 'C:\\MSSQL_Backups_Zimmermann\\'
	DECLARE @DatabaseName NVARCHAR(255)
	DECLARE @BackupCommand NVARCHAR(500)
	DECLARE @CheckpointCommand NVARCHAR(50)
	DECLARE db_cursor CURSOR FOR
	SELECT name 
	FROM sys.databases
	WHERE name NOT IN ('master', 'model', 'msdb', 'tempdb') -- Optional: Systemdatenbanken ausschließen
	OPEN db_cursor   
	FETCH NEXT FROM db_cursor INTO @DatabaseName   
	WHILE @@FETCH_STATUS = 0   
	BEGIN   
		-- Backup-Befehl
		SET @BackupCommand = 'BACKUP DATABASE [' + @DatabaseName + '] TO DISK = ''' + @BackupPath + @DatabaseName + '.bak'''
		EXEC sp_executesql @BackupCommand
		-- Checkpoint-Befehl
		SET @CheckpointCommand = 'CHECKPOINT'
		EXEC sp_executesql @CheckpointCommand
		-- Nächster Datensatz
		FETCH NEXT FROM db_cursor INTO @DatabaseName   
	END   
	CLOSE db_cursor   
	DEALLOCATE db_cursor

END

BEGIN --alle gesicherten Datenbanken eines Servers aus definierten Pfad anhand des Dateinamens wiederherstellen - nur vorhandene Datenbanken wiederherstellen ✅

	-- Wiederherstellung der Datenbanken aus dem Backup-Verzeichnis
	DECLARE @BackupPath NVARCHAR(255) = 'C:\\MSSQL_Backups_Zimmermann\\'
	DECLARE @DatabaseName NVARCHAR(255)
	DECLARE @RestoreCommand NVARCHAR(500)
	DECLARE @LogicalName NVARCHAR(255)
	DECLARE db_cursor CURSOR FOR
	SELECT name 
	FROM sys.databases
	WHERE name NOT IN ('master', 'model', 'msdb', 'tempdb') -- Optional: Systemdatenbanken ausschließen
	OPEN db_cursor   
	FETCH NEXT FROM db_cursor INTO @DatabaseName   
	WHILE @@FETCH_STATUS = 0   
	BEGIN   
		-- Erstelle Restore-Befehl für jede Datenbank
		SET @RestoreCommand = 'RESTORE DATABASE [' + @DatabaseName + '] FROM DISK = ''' + @BackupPath + @DatabaseName + '.bak'' WITH REPLACE'
		-- Führe Restore-Befehl aus
		EXEC sp_executesql @RestoreCommand
		-- Nächster Datensatz
		FETCH NEXT FROM db_cursor INTO @DatabaseName   
	END   
	CLOSE db_cursor   
	DEALLOCATE db_cursor

END

BEGIN --alle gesicherten Datenbanken eines Servers aus definierten Pfad anhand des Dateinamens wiederherstellen - nicht vorhandene Datenbanken erzeugen ✅

	-- Schritt 1: Konfiguration aktivieren
	EXEC sp_configure 'show advanced options', 1;
	RECONFIGURE;
	EXEC sp_configure 'xp_cmdshell', 1;
	RECONFIGURE;
	-- Schritt 2: Backup-Pfad definieren
	DECLARE @BackupPath NVARCHAR(255) = 'C:\MSSQL_Backups_Zimmermann\''
	DECLARE @Command NVARCHAR(255)
	DECLARE @DatabaseName NVARCHAR(255)
	-- Schritt 3: Temporäre Tabelle zur Speicherung der Backup-Dateien erstellen
	CREATE TABLE #BackupFiles (FileName NVARCHAR(255))
	-- Schritt 4: Backup-Dateien im angegebenen Verzeichnis auflisten
	SET @Command = 'dir "' + @BackupPath + '" /b *.bak'
	INSERT INTO #BackupFiles (FileName)
	EXEC xp_cmdshell @Command
	-- Schritt 5: Wiederherstellung der Datenbanken aus dem Backup-Verzeichnis
	DECLARE @RestoreCommand NVARCHAR(500)
	DECLARE db_cursor CURSOR FOR
	SELECT FileName
	FROM #BackupFiles
	WHERE FileName IS NOT NULL
	OPEN db_cursor   
	FETCH NEXT FROM db_cursor INTO @DatabaseName   
	WHILE @@FETCH_STATUS = 0   
	BEGIN   
		-- Erstelle Restore-Befehl für jede Backup-Datei
		SET @RestoreCommand = 'RESTORE DATABASE [' + LEFT(@DatabaseName, LEN(@DatabaseName) - 4) + '] FROM DISK = ''' + @BackupPath + @DatabaseName + ''' WITH REPLACE'
		-- Führe Restore-Befehl aus
		EXEC sp_executesql @RestoreCommand
		-- Nächster Datensatz
		FETCH NEXT FROM db_cursor INTO @DatabaseName   
	END   
	CLOSE db_cursor   
	DEALLOCATE db_cursor
	-- Schritt 6: Temporäre Tabelle aufräumen
	DROP TABLE #BackupFiles

END

BEGIN --Herausfinden ob ein Trigger von einer Sequenz oder Tabelle ausgelöst wird (POSTGRES) ✅

	CREATE OR REPLACE FUNCTION my_trigger_function()
RETURNS trigger AS $$
DECLARE
    object_type TEXT;
BEGIN
    -- Bestimme den Typ des Zielobjekts (Tabelle oder Sequenz)
    SELECT CASE
               WHEN EXISTS (SELECT 1 FROM pg_class WHERE relname = TG_RELNAME AND relkind = 'r') THEN 'table'
               WHEN EXISTS (SELECT 1 FROM pg_class WHERE relname = TG_RELNAME AND relkind = 'S') THEN 'sequence'
               ELSE 'unknown'
           END INTO object_type;
    -- Beispielhafte Fallunterscheidung
    IF object_type = 'table' THEN
        RAISE NOTICE 'The object % is a table.', TG_RELNAME;
        -- Hier Code für den Fall, dass es sich um eine Tabelle handelt
    ELSIF object_type = 'sequence' THEN
        RAISE NOTICE 'The object % is a sequence.', TG_RELNAME;
        -- Hier Code für den Fall, dass es sich um eine Sequenz handelt
    ELSE
        RAISE EXCEPTION 'The object type of % is not recognized.', TG_RELNAME;
    END IF;
    RETURN NEW;
	END;
	$$ LANGUAGE plpgsql;

END
