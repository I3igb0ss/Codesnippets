﻿Imports System.Runtime.InteropServices
Imports System.Drawing

Public Class WindowHelper
    <DllImport("user32.dll")>
    Private Shared Function GetWindowRect(hWnd As IntPtr, ByRef lpRect As RECT) As Boolean
    End Function

    <StructLayout(LayoutKind.Sequential)>
    Private Structure RECT
        Public Left As Integer
        Public Top As Integer
        Public Right As Integer
        Public Bottom As Integer
    End Structure

    Public Shared Function GetExactBounds(form As Form) As Rectangle
        Dim rect As RECT
        If GetWindowRect(form.Handle, rect) Then
            Return New Rectangle(rect.Left, rect.Top, rect.Right - rect.Left, rect.Bottom - rect.Top)
        Else
            Throw New InvalidOperationException("GetWindowRect konnte die Fensterkoordinaten nicht abrufen.")
        End If
    End Function
End Class