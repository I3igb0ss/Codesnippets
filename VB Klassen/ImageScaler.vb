﻿Public Class ImageScaler
    ' Methode zum Skalieren eines Bildes auf eine feste Breite, wobei das Seitenverhältnis beibehalten wird
    Public Shared Function ScaleImageByWidth(ByVal originalImage As Image, ByVal targetWidth As Integer) As Image
        ' Das Seitenverhältnis berechnen (Breite/Höhe)
        Dim aspectRatio As Double = CDbl(originalImage.Width) / CDbl(originalImage.Height)
        
        ' Höhe proportional zur angegebenen Breite berechnen
        Dim targetHeight As Integer = CInt(targetWidth / aspectRatio)

        ' Das Bild auf die neue Breite und Höhe skalieren
        Return ScaleImage(originalImage, targetWidth, targetHeight)
    End Function

    ' Methode zum Skalieren eines Bildes auf eine feste Breite und Höhe
    Public Shared Function ScaleImage(ByVal originalImage As Image, ByVal targetWidth As Integer, ByVal targetHeight As Integer) As Image
        ' Neues Bitmap in der gewünschten Größe erstellen
        Dim scaledImage As New Bitmap(targetWidth, targetHeight)

        ' Grafik-Objekt verwenden, um das ursprüngliche Bild auf die neue Größe zu zeichnen
        Using g As Graphics = Graphics.FromImage(scaledImage)
            g.DrawImage(originalImage, 0, 0, targetWidth, targetHeight)
        End Using

        ' Skaliertes Bild zurückgeben
        Return scaledImage
    End Function

    ' Methode zum Setzen des skalierten Bildes als Hintergrundbild eines Controls
    Public Shared Sub SetScaledBackgroundImage(ByVal control As Control, ByVal image As Image, ByVal width As Integer, ByVal height As Integer)
        ' Bild skalieren
        Dim scaledImage As Image = ScaleImage(image, width, height)

        ' Skaliertes Bild als Hintergrundbild setzen
        control.BackgroundImage = scaledImage
        control.BackgroundImageLayout = ImageLayout.Center ' Optional: Layout beibehalten
    End Sub

    ' Methode zum Setzen des proportional skalierten Bildes basierend auf der Breite als Hintergrundbild eines Controls
    Public Shared Sub SetScaledBackgroundImageByWidth(ByVal control As Control, ByVal image As Image, ByVal width As Integer)
        ' Bild proportional zur Breite skalieren
        Dim scaledImage As Image = ScaleImageByWidth(image, width)

        ' Skaliertes Bild als Hintergrundbild setzen
        control.BackgroundImage = scaledImage
        control.BackgroundImageLayout = ImageLayout.Center ' Optional: Layout beibehalten
    End Sub
End Class