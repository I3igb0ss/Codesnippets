﻿Imports System.Runtime.InteropServices
Imports System.Collections.Generic
Imports System.Windows.Forms
Imports System.Management

Public Class MonitorInfo
    ' Display Device Struktur für EnumDisplayDevices
    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Auto)>
    Private Structure DISPLAY_DEVICE
        Public cb As Integer
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=32)>
        Public DeviceName As String
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=128)>
        Public DeviceString As String
        Public StateFlags As Integer
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=128)>
        Public DeviceID As String
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=128)>
        Public DeviceKey As String
    End Structure

    ' Import der EnumDisplayDevices Funktion
    <DllImport("user32.dll", CharSet:=CharSet.Auto)>
    Private Shared Function EnumDisplayDevices(
        ByVal lpDevice As String,
        ByVal iDevNum As Integer,
        ByRef lpDisplayDevice As DISPLAY_DEVICE,
        ByVal dwFlags As Integer) As Boolean
    End Function

    ' Dictionary zur Speicherung von Friendly Names und Instanznamen
    Private Shared BildschirmNamen As New Dictionary(Of String, (FriendlyName As String, DeviceID As String))

    ' Initialisiert und lädt alle Bildschirminformationen
    Public Shared Sub Initialize()
        BildschirmNamen.Clear()
        Dim devNum As Integer = 0
        Dim displayDevice As DISPLAY_DEVICE = New DISPLAY_DEVICE()
        displayDevice.cb = Marshal.SizeOf(displayDevice)

        ' Erster Durchlauf zur Ermittlung aller Bildschirme
        While EnumDisplayDevices(Nothing, devNum, displayDevice, 0)
            Dim monitorDevice As DISPLAY_DEVICE = New DISPLAY_DEVICE()
            monitorDevice.cb = Marshal.SizeOf(monitorDevice)

            ' Zweiter Aufruf zur Ermittlung des Friendly Names und DeviceID
            If EnumDisplayDevices(displayDevice.DeviceName, 0, monitorDevice, 0) Then
                BildschirmNamen(displayDevice.DeviceName) = (monitorDevice.DeviceString, monitorDevice.DeviceID)
            End If

            devNum += 1
            displayDevice.cb = Marshal.SizeOf(displayDevice)
        End While
    End Sub

    ' Gibt den Friendly Name für ein Screen-Objekt zurück
    Public Shared Function GetFriendlyName(screen As Screen) As String
        ' Initialisiere Bildschirmnamen bei erstem Zugriff
        If BildschirmNamen.Count = 0 Then
            Initialize()
        End If

        ' Hole den Friendly Name basierend auf DeviceName
        Dim deviceName As String = screen.DeviceName
        If BildschirmNamen.ContainsKey(deviceName) Then
            Return BildschirmNamen(deviceName).FriendlyName
        Else
            Return "Unbekannt"
        End If
    End Function

    ' Gibt die Seriennummer eines Bildschirms zurück
    Public Shared Function GetSerialNumber(screen As Screen) As String
        Try
            ' WMI Abfrage für die Seriennummer
            Dim searcher As New ManagementObjectSearcher("root\WMI", "SELECT * FROM WmiMonitorID")
            For Each queryObj As ManagementObject In searcher.Get()
                ' Überprüfen, ob die Seriennummer zum Bildschirm passt
                Dim nameArray As Byte() = CType(queryObj("SerialNumberID"), Byte())
                Dim serial As String = ByteArrayToString(nameArray)
                Return serial
            Next
        Catch ex As Exception
            ' Falls keine Seriennummer gefunden wird
        End Try

        Return "Keine Seriennummer gefunden"
    End Function

    ' Gibt den Instanznamen eines Bildschirms zurück
    Public Shared Function GetInstanceName(screen As Screen) As String
        ' Initialisiere Bildschirmnamen bei erstem Zugriff
        If BildschirmNamen.Count = 0 Then
            Initialize()
        End If

        ' Hole den Instanznamen (DeviceID) basierend auf DeviceName
        Dim deviceName As String = screen.DeviceName
        If BildschirmNamen.ContainsKey(deviceName) Then
            Return BildschirmNamen(deviceName).DeviceID
        Else
            Return "Unbekannt"
        End If
    End Function

    ' Gibt nur den Teil des Instanznamens zwischen den ersten beiden Schrägstrichen zurück
    Public Shared Function GetInstanceNameType(screen As Screen) As String
        Dim instanceName As String = GetInstanceName(screen)
        
        ' Splitte den Instanznamen an den Schrägstrichen und gib den mittleren Teil zurück
        Dim parts As String() = instanceName.Split("\"c)

        If parts.Length >= 3 Then
            Return parts(1)  ' Gibt den Teil zwischen den ersten beiden Schrägstrichen zurück
        Else
            Return "Unbekannt"
        End If
    End Function

    ' Hilfsfunktion zur Umwandlung eines Byte-Arrays in einen lesbaren String
    Private Shared Function ByteArrayToString(bytes As Byte()) As String
        Dim result As String = ""
        For Each b As Byte In bytes
            If b <> 0 Then
                result &= Convert.ToChar(b)
            End If
        Next
        Return result
    End Function
End Class