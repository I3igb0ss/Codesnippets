﻿Imports System.Diagnostics
Imports System.Runtime.InteropServices

Public Class ProcessHelper

    <DllImport("kernel32.dll")>
    Private Shared Function OpenProcess(dwDesiredAccess As Integer, bInheritHandle As Boolean, dwProcessId As Integer) As IntPtr
    End Function

    <DllImport("ntdll.dll", SetLastError:=True)>
    Private Shared Function NtQueryInformationProcess(
        processHandle As IntPtr,
        processInformationClass As Integer,
        ByRef processInformation As PROCESS_BASIC_INFORMATION,
        processInformationLength As Integer,
        ByRef returnLength As Integer) As Integer
    End Function

    <DllImport("kernel32.dll")>
    Private Shared Function CloseHandle(hObject As IntPtr) As Boolean
    End Function

    <StructLayout(LayoutKind.Sequential)>
    Private Structure PROCESS_BASIC_INFORMATION
        Public Reserved1 As IntPtr
        Public PebBaseAddress As IntPtr
        Public Reserved2 As IntPtr
        Public UniqueProcessId As IntPtr
        Public InheritedFromUniqueProcessId As IntPtr
    End Structure

    Private Const PROCESS_QUERY_INFORMATION As Integer = &H400
    Private Const PROCESS_VM_READ As Integer = &H10

    Public Shared Function IsProcessRunning(processName As String) As Boolean
        Dim currentProcessId = Process.GetCurrentProcess().Id
        Dim mainProgramPath As String = Process.GetCurrentProcess().MainModule.FileName
        Dim mainProgramDirectory As String = IO.Path.GetDirectoryName(mainProgramPath)

        ' Holen Sie alle laufenden Prozesse mit dem angegebenen Namen
        Dim processes = Process.GetProcessesByName(processName)

        Dim hasExternalProcessFromSameDirectory As Boolean = False
        Dim hasAnyExternalProcess As Boolean = False

        For Each proc In processes
            Dim parentId = GetParentProcessId(proc.Id)

            Try
                ' Hole den Pfad des Prozesses
                Dim processPath As String = proc.MainModule.FileName
                Dim processDirectory As String = IO.Path.GetDirectoryName(processPath)

                ' Prüfen, ob Prozess von der aktuellen Instanz stammt
                If parentId.HasValue AndAlso parentId.Value = currentProcessId Then
                    ' Eigene Instanz, ignorieren
                    Continue For
                End If

                ' Es gibt mindestens einen fremden Prozess
                hasAnyExternalProcess = True

                ' Prüfen, ob Prozess aus dem gleichen Verzeichnis stammt
                If String.Equals(processDirectory, mainProgramDirectory, StringComparison.OrdinalIgnoreCase) Then
                    hasExternalProcessFromSameDirectory = True
                End If
            Catch ex As Exception
                ' Zugriff auf MainModule kann fehlschlagen, überspringen
                Continue For
            End Try
        Next

        ' Rückgabebedingung:
        ' - True, wenn ein fremder Prozess aus dem gleichen Verzeichnis gefunden wurde.
        ' - False, wenn keine fremden Prozesse oder nur fremde Prozesse aus anderen Verzeichnissen laufen.
        Return hasExternalProcessFromSameDirectory
    End Function

    Private Shared Function GetParentProcessId(processId As Integer) As Integer?
        Dim hProcess = OpenProcess(PROCESS_QUERY_INFORMATION Or PROCESS_VM_READ, False, processId)

        If hProcess = IntPtr.Zero Then
            Return Nothing
        End If

        Try
            Dim pbi As PROCESS_BASIC_INFORMATION
            Dim returnLength As Integer
            Dim status = NtQueryInformationProcess(hProcess, 0, pbi, Marshal.SizeOf(pbi), returnLength)

            If status = 0 Then ' NT_SUCCESS
                Return pbi.InheritedFromUniqueProcessId.ToInt32()
            Else
                Return Nothing
            End If
        Finally
            CloseHandle(hProcess)
        End Try
    End Function

End Class