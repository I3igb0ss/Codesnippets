﻿Imports System.Runtime.InteropServices
Imports System.Windows.Forms

Public Class SnapLayoutHelper

    ' Timer für die Überprüfung der Fenstergröße
    Private WithEvents snapLayoutCheckTimer As New Timer()
    Private lastWindowSize As Size
    Private Const SNAP_LAYOUT_THRESHOLD As Integer = 30 ' Mindestgröße der Änderung, um als Snap zu zählen
    Private _form As Form ' Referenz auf das Form, mit dem gearbeitet wird

    ' Import für DwmGetWindowAttribute und Konstante für die erweiterte Fensterrahmenabfrage
    <DllImport("dwmapi.dll", CharSet:=CharSet.Auto)>
    Private Shared Function DwmGetWindowAttribute(hWnd As IntPtr, dwAttribute As Integer, ByRef pvAttribute As RECT, cbAttribute As Integer) As Integer
    End Function

    ' Konstante für DwmGetWindowAttribute
    Private Const DWMWA_EXTENDED_FRAME_BOUNDS As Integer = 9

    ' Struktur für Fensterrahmen-Position und Größe
    <StructLayout(LayoutKind.Sequential)>
    Private Structure RECT
        Public Left As Integer
        Public Top As Integer
        Public Right As Integer
        Public Bottom As Integer
    End Structure

    ' Konstruktor
    Public Sub New(form As Form)
        _form = form
        lastWindowSize = form.Size
        snapLayoutCheckTimer.Interval = 500 ' 500 ms Verzögerung
        AddHandler _form.SizeChanged, AddressOf frmZentral_SizeChanged
    End Sub

    ' Form Load - Initialisieren des Timers und Variablen
    Public Sub StartMonitoring()
        snapLayoutCheckTimer.Start()
    End Sub

    ' Überwachen der Fenstergrößenänderung und Starten des Timers
    Private Sub frmZentral_SizeChanged(sender As Object, e As EventArgs)
        ' Wenn sich die Fenstergröße signifikant geändert hat, starten wir den Timer
        If Math.Abs(_form.Width - lastWindowSize.Width) > SNAP_LAYOUT_THRESHOLD OrElse
           Math.Abs(_form.Height - lastWindowSize.Height) > SNAP_LAYOUT_THRESHOLD Then
            snapLayoutCheckTimer.Start()
        End If

        ' Fenstergröße speichern für spätere Vergleiche
        lastWindowSize = _form.Size
    End Sub

    ' Der Timer prüft auf Snap Layouts
    Private Sub snapLayoutCheckTimer_Tick(sender As Object, e As EventArgs) Handles snapLayoutCheckTimer.Tick
        snapLayoutCheckTimer.Stop() ' Timer stoppen, um mehrfaches Auslösen zu verhindern

        ' Überprüfen auf Snap Layout, aber Maximieren oder Vollbild ignorieren
        If DetectSnapLayout() AndAlso Not IsMaximizedOrFullscreen() Then
            ' Hier Snap Layout erkannt, z. B. Fenster auf ein Layout gesnappt
            RaiseEvent SnapLayoutDetected(Me, EventArgs.Empty)
        Else
            ' Kein Snap Layout erkannt oder Fenster ist maximiert/vollbild
            RaiseEvent SnapLayoutNotDetected(Me, EventArgs.Empty)
        End If
    End Sub

    ' Überprüfen auf Snap Layout
    Private Function DetectSnapLayout() As Boolean
        ' Wir verwenden DwmGetWindowAttribute, um die aktuelle Position des Fensters zu erhalten
        Dim currentBounds As New RECT()
        Dim result = DwmGetWindowAttribute(_form.Handle, DWMWA_EXTENDED_FRAME_BOUNDS, currentBounds, Marshal.SizeOf(GetType(RECT)))

        If result <> 0 Then
            ' Fehler beim Abrufen der Fensterattribute
            Return False
        End If

        ' Prüfen ob das Fenster an einer bekannten Snap-Position ist
        Dim screenBounds = Screen.FromHandle(_form.Handle).WorkingArea
        If Math.Abs(currentBounds.Left - screenBounds.Left) < 10 OrElse
           Math.Abs(currentBounds.Top - screenBounds.Top) < 10 Then
            Return True ' Fenster ist gesnappt
        End If

        Return False
    End Function

    ' Funktion zur Überprüfung, ob das Fenster maximiert oder im Vollbildmodus ist
    Private Function IsMaximizedOrFullscreen() As Boolean
        ' Prüfen auf Maximierung oder Vollbild
        Return _form.WindowState = FormWindowState.Maximized OrElse _form.WindowState = FormWindowState.Normal AndAlso _form.Top = 0 AndAlso _form.Left = 0
    End Function

    ' Ereignis für erkannte Snap Layouts
    Public Event SnapLayoutDetected As EventHandler
    Public Event SnapLayoutNotDetected As EventHandler

End Class