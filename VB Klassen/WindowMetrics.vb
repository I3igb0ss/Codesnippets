﻿Imports System.Runtime.InteropServices
Imports System.Drawing

Public NotInheritable Class WindowMetrics

    ' Deklariere die Funktion aus der User32.dll
    <DllImport("user32.dll")>
    Private Shared Function GetSystemMetrics(nIndex As Integer) As Integer
    End Function

    ' Konstanten für GetSystemMetrics
    Private Const SM_CYCAPTION As Integer = 4     ' Höhe der Titelleiste
    Private Const SM_CXSIZEFRAME As Integer = 32  ' Breite des Rahmens (links und rechts)
    Private Const SM_CYSIZEFRAME As Integer = 33  ' Höhe des Rahmens (oben und unten)

    ''' <summary>
    ''' Gibt die Breite des linken und rechten Fensterrahmens zurück.
    ''' </summary>
    Public Shared Function GetFrameWidth() As Integer
        Return 2 * GetSystemMetrics(SM_CXSIZEFRAME)
    End Function

    ''' <summary>
    ''' Gibt die Höhe des oberen und unteren Fensterrahmens sowie der Titelleiste zurück.
    ''' </summary>
    Public Shared Function GetFrameHeight() As Integer
        Return GetSystemMetrics(SM_CYCAPTION) + 2 * GetSystemMetrics(SM_CYSIZEFRAME)
    End Function

    ''' <summary>
    ''' Gibt die gesamte Breite der Anwendung zurück, einschließlich Rahmen und Padding.
    ''' </summary>
    Public Shared Function GetTotalWindowWidth(form As Form) As Integer
        Dim paddingWidth As Integer = form.Padding.Left + form.Padding.Right
        Dim borderWidth As Integer = SystemInformation.BorderSize.Width * 2
        Return form.ClientSize.Width + GetFrameWidth() + paddingWidth + borderWidth
    End Function

    ''' <summary>
    ''' Gibt die gesamte Höhe der Anwendung zurück, einschließlich Titelleiste, Rahmen und Padding.
    ''' </summary>
    Public Shared Function GetTotalWindowHeight(form As Form) As Integer
        Dim paddingHeight As Integer = form.Padding.Top + form.Padding.Bottom
        Dim borderHeight As Integer = SystemInformation.BorderSize.Height * 2
        Return form.ClientSize.Height + GetFrameHeight() + paddingHeight + borderHeight
    End Function

    ''' <summary>
    ''' Gibt ein Rectangle zurück, das die genauen Abmessungen und Positionen der Form enthält.
    ''' </summary>
    ''' <param name="form">Die Referenz auf die Form.</param>
    ''' <returns>Ein Rectangle mit den genauen Dimensionen und Positionen der Form.</returns>
    Public Shared Function GetExactBounds(form As Form) As Rectangle
        Dim totalWidth As Integer = GetTotalWindowWidth(form)
        Dim totalHeight As Integer = GetTotalWindowHeight(form)
        Return New Rectangle(form.Left, form.Top, totalWidth, totalHeight)
    End Function

End Class