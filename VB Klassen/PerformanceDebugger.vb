﻿Imports System.IO
Imports System.Diagnostics
Imports System.Windows.Forms

Public Class PerformanceDebugger
    ' Dictionary zur Speicherung der Startzeitpunkte verschiedener Methoden
    Private Shared startTimes As New Dictionary(Of String, Stopwatch)

    ' Der Pfad zur Debug-Datei auf dem Desktop
    Private Shared ReadOnly logFilePath As String = Path.Combine(
        Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
        "PGIS_Performance_Debugging.txt"
    )

    ' Methode zum Starten der Zeitmessung (am Anfang der zu messenden Methode aufrufen)
    Public Shared Sub StartMeasurement(methodName As String)
        Dim stopwatch As New Stopwatch()
        stopwatch.Start()
        startTimes(methodName) = stopwatch
    End Sub

    ' Methode zum Stoppen der Zeitmessung und Speichern des Ergebnisses in der Datei
    Public Shared Sub StopMeasurement(methodName As String)
        ' Prüfen, ob die Methode im Dictionary vorhanden ist
        If startTimes.ContainsKey(methodName) Then
            ' Zeit stoppen
            Dim stopwatch As Stopwatch = startTimes(methodName)
            stopwatch.Stop()
            Dim elapsedTime As TimeSpan = stopwatch.Elapsed
            startTimes.Remove(methodName) ' Entferne den Eintrag, um Speicher freizugeben

            ' Textzeile vorbereiten
            Dim logEntry As String = $"{DateTime.Now:yyyy-MM-dd HH:mm:ss} | Methode: {methodName} | Dauer: {elapsedTime.TotalMilliseconds} ms{Environment.NewLine}"

            ' In die Datei schreiben (anhängen)
            Try
                File.AppendAllText(logFilePath, logEntry)
            Catch ex As Exception
                MessageBox.Show($"Fehler beim Schreiben in die Log-Datei: {ex.Message}", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        Else
            ' Warnung anzeigen, falls StartMeasurement nicht aufgerufen wurde
            MessageBox.Show($"Warnung: Keine Startzeit für die Methode '{methodName}' gefunden.", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub
End Class