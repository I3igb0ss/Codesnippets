﻿Imports System.IO
Imports Newtonsoft.Json.Linq
Imports System.Windows.Forms
Imports System.Drawing

Public Class FancyZonesReader
    ' Die Methode liest die Zoneninformationen für den spezifischen Bildschirm aus
    Public Shared Function GetZones(screen As Screen) As List(Of Rectangle)
        Try
            Dim instanceNameType As String = MonitorInfo.GetInstanceNameType(screen)

            ' Lade die Pfade für die JSON-Dateien
            Dim appliedLayoutsPath As String = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Microsoft\PowerToys\FancyZones\applied-layouts.json")
            Dim customLayoutsPath As String = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Microsoft\PowerToys\FancyZones\custom-layouts.json")

            ' Überprüfen, ob die JSON-Dateien existieren
            If Not File.Exists(appliedLayoutsPath) OrElse Not File.Exists(customLayoutsPath) Then
                Return New List(Of Rectangle)() ' Rückgabe einer leeren Liste, wenn Dateien nicht existieren
            End If

            ' Lese und parse die JSON-Dateien
            Dim appliedLayoutsJson As JObject = JObject.Parse(File.ReadAllText(appliedLayoutsPath))
            Dim customLayoutsJson As JObject = JObject.Parse(File.ReadAllText(customLayoutsPath))

            ' Durchsuche alle Layouts in applied-layouts.json
            Dim customLayoutConfig As JObject = Nothing
            For Each appliedLayout In appliedLayoutsJson("applied-layouts")
                ' Lese die UUID aus applied-layout
                Dim appliedLayoutUuid As String = appliedLayout("applied-layout")("uuid").ToString()

                ' Durchlaufe custom-layouts und vergleiche die UUID mit der aus applied-layouts
                For Each customLayout In customLayoutsJson("custom-layouts")
                    If customLayout("uuid").ToString() = appliedLayoutUuid Then
                        customLayoutConfig = customLayout
                        Exit For ' Passendes Layout gefunden, Schleife beenden
                    End If
                Next

                ' Wenn wir ein passendes Layout gefunden haben, brechen wir ab
                If customLayoutConfig IsNot Nothing Then
                    Exit For
                End If
            Next

            ' Wenn kein Layout gefunden wurde, gebe eine leere Liste zurück
            If customLayoutConfig Is Nothing Then
                Return New List(Of Rectangle)() ' Rückgabe einer leeren Liste, wenn kein Layout gefunden wurde
            End If

            ' Hole die Prozentwerte der Spalten und den Abstand aus dem Layout
            Dim columnsPercentage As List(Of Integer) = customLayoutConfig("info")("columns-percentage").Select(Function(p) p.ToObject(Of Integer)()).ToList()
            Dim spacing As Integer = customLayoutConfig("info")("spacing").ToObject(Of Integer)()

            ' Berechne die Zonen basierend auf der Working Area
            Return CalculateZoneBounds(screen.Bounds, columnsPercentage, spacing)
        Catch ex As Exception
            ' Fange alle anderen Fehler ab und gebe eine leere Liste zurück
            Return New List(Of Rectangle)() ' Rückgabe einer leeren Liste bei einem Fehler
        End Try
    End Function

    ' Berechnet die Zonen unter Berücksichtigung der Working Area des Monitors
    Private Shared Function CalculateZoneBounds(screenBounds As Rectangle, columnPercentages As List(Of Integer), spacing As Integer) As List(Of Rectangle)
    ' Hole die Working Area des Bildschirms
    Dim workingArea As Rectangle = Screen.FromRectangle(screenBounds).WorkingArea

    ' Gesamtbreite und -höhe der Working Area
    Dim totalWidth As Integer = workingArea.Width
    Dim totalHeight As Integer = workingArea.Height - 2 * spacing ' Spacings oben und unten abziehen

    ' Initialisiere die Liste der Zonen
    Dim zoneBounds As New List(Of Rectangle)
    Dim currentX As Integer = workingArea.X + spacing ' Start X-Position (inkl. linker Abstand)

    ' Berechne jede Zone
    For i As Integer = 0 To columnPercentages.Count - 1
        ' Berechne die Zone-Breite
        Dim zoneWidth As Integer
        If i = 0 OrElse i = columnPercentages.Count - 1 Then
            ' Äußere Zonen: nur eine Seite bekommt volle Gap Size
            zoneWidth = CInt((columnPercentages(i) / 10000.0) * totalWidth) - spacing - spacing \ 2
        Else
            ' Innere Zonen: beide Seiten bekommen je eine halbe Gap Size
            zoneWidth = CInt((columnPercentages(i) / 10000.0) * totalWidth) - spacing
        End If

        ' Füge die Zone der Liste hinzu
        zoneBounds.Add(New Rectangle(currentX, workingArea.Y + spacing, zoneWidth, totalHeight))

        ' Update der X-Position: aktuelle Breite + ein halbes Spacing nach rechts
        currentX += zoneWidth + spacing
    Next

    Return zoneBounds
End Function
End Class