﻿Public Class MonitorHelper
    Public Shared Function GetCleanInstanceName(instanceName As String) As String
        ' Entfernt "DISPLAY\" und den Präfix bis zum ersten "\" (z.B. "TCL0000\")
        Dim cleanName As String = instanceName

        If cleanName.StartsWith("DISPLAY\") Then
            cleanName = cleanName.Substring("DISPLAY\".Length)
        End If

        If cleanName.Contains("\") Then
            cleanName = cleanName.Substring(cleanName.IndexOf("\") + 1)
        End If

        ' Entfernt den Unterstrich und alles, was danach folgt
        If cleanName.Contains("_") Then
            cleanName = cleanName.Substring(0, cleanName.LastIndexOf("_"c))
        End If

        Return cleanName
    End Function

    Public Shared Function GetDisplayToCleanInstanceMapping() As Dictionary(Of String, String)
        Dim mapping As New Dictionary(Of String, String)()

        Dim searcher As New ManagementObjectSearcher("root\wmi", "SELECT * FROM WmiMonitorID")
        For Each queryObj As ManagementObject In searcher.Get()
            Dim instanceName As String = queryObj("InstanceName").ToString()
            Dim cleanName As String = GetCleanInstanceName(instanceName)

            If Not mapping.ContainsKey(instanceName) Then
                mapping.Add(instanceName, cleanName)
            End If
        Next

        Return mapping
    End Function
End Class